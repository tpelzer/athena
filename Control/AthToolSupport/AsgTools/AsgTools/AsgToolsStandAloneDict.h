// Dear emacs, this is -*- c++ -*-
// $Id: AsgToolsStandAloneDict.h  $
#ifndef ASGTOOLS_ASGTOOLSSTANDALONEDICT_H
#define ASGTOOLS_ASGTOOLSSTANDALONEDICT_H

#ifdef __GNUC__
# pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#endif

// In athena, we get this from GAUDI
#include "AsgTools/StatusCode.h"

#endif // not ASGTOOLS_ASGTOOLSSTANDALONEDICT_H
