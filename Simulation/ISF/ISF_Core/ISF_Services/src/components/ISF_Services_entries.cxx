#include "../AFIIEnvelopeDefSvc.h"
#include "../GeoIDSvc.h"
#include "../InputConverter.h"
#include "../ISFEnvelopeDefSvc.h"
#include "../ParticleBrokerDynamicOnReadIn.h"
#include "../ParticleKillerSimSvc.h"
#include "../TruthSvc.h"

DECLARE_COMPONENT( ISF::AFIIEnvelopeDefSvc )
DECLARE_COMPONENT( ISF::GeoIDSvc )
DECLARE_COMPONENT( ISF::InputConverter )
DECLARE_COMPONENT( ISF::ISFEnvelopeDefSvc )
DECLARE_COMPONENT( ISF::ParticleBrokerDynamicOnReadIn )
DECLARE_COMPONENT( ISF::ParticleKillerSimSvc )
DECLARE_COMPONENT( ISF::TruthSvc )

